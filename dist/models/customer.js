"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerFactory = exports.Customer = void 0;
const sequelize_1 = require("sequelize");
class Customer extends sequelize_1.Model {
}
exports.Customer = Customer;
function CustomerFactory(sequelize) {
    return sequelize.define('customer', {
        id: {
            field: 'id',
            type: sequelize_1.DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        name: {
            field: 'name',
            type: sequelize_1.DataTypes.STRING,
            allowNull: true,
        },
        password: {
            field: 'password',
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        email: {
            field: 'email',
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
    }, {
        timestamps: false,
        freezeTableName: true,
    });
}
exports.CustomerFactory = CustomerFactory;
