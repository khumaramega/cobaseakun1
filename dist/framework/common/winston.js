"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_root_path_1 = __importDefault(require("app-root-path"));
const winston_1 = __importDefault(require("winston"));
//winston is designed to be a simple and universal logging library with support for multiple transports
//winston aims to decouple parts of the logging process to make it more flexible and extensible.
// Attention is given to supporting flexibility in log formatting (see: Formats) & levels (see: Using custom logging levels),
//, and ensuring those APIs decoupled from the implementation of transport logging (i.e. how the logs are stored / indexed, see: Adding Custom Transports) to the API that they exposed to the programmer.
//liat efek adanya winston dmna ?
const options = {
    file: {
        level: 'info',
        filename: `${app_root_path_1.default}/logs/app.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880,
        maxFiles: 5,
        colorize: false,
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
};
const logger = winston_1.default.createLogger({
    transports: [new winston_1.default.transports.File(options.file), new winston_1.default.transports.Console(options.console)],
    exitOnError: false,
});
exports.default = logger;
