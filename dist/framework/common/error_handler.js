"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const winston_1 = __importDefault(require("./winston"));
const sequelize_1 = require("sequelize");
const constant_1 = require("./constant");
const ErrorHandler = (err, req, res, next) => {
    let message, status, code;
    if (err instanceof sequelize_1.Error) {
        if (process.env.DEBUG) {
            status = constant_1.HttpCode.INTERNAL_SERVER_ERROR;
            message = err.message;
        }
        else {
            status = constant_1.HttpCode.INTERNAL_SERVER_ERROR;
            message = 'internal server error';
        }
    }
    else {
        if (process.env.DEBUG) {
            status = err.status;
            message = err.message;
        }
        else {
            status = constant_1.HttpCode.INTERNAL_SERVER_ERROR;
            message = 'oops something wrong';
        }
    }
    res.locals.message = message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    winston_1.default.error(`${status || 500} - ${message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
    res.status(status || 500);
    res.json({
        status: status || 500,
        message: message,
        code: code,
    });
};
exports.default = ErrorHandler;
