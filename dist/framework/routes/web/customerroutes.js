"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mysql1_1 = __importDefault(require("../../../domains/customer/repository/mysql1"));
const express_1 = __importDefault(require("express"));
//import {Request, Response} from 'express';
const PackageRouter = (dep) => {
    const router = express_1.default.Router();
    const controller = mysql1_1.default(dep.Mysql);
    router.route('/').get(controller.Read);
    router.route('/:id').get(controller.ReadBy);
    router.route('/cust').get(controller.Create);
    return router;
};
exports.default = PackageRouter;
