"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const handler_1 = __importDefault(require("../../../domains/customer/delivery/http/handler"));
const express_1 = __importDefault(require("express"));
const PackageRouter = (dep) => {
    const router = express_1.default.Router();
    const controller = handler_1.default(dep);
    router.route('/').get(handler_1.default(dep).GetCustomer);
    //	router.route('/:id').get(controller.ReadBy) 
    return router;
};
exports.default = PackageRouter;
