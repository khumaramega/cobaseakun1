"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const customer_1 = __importDefault(require("./customer"));
//import Customer from './customerroutes';
const express_1 = __importDefault(require("express"));
const apiRouter = (dep) => {
    const routes = express_1.default.Router();
    const customerRouter = customer_1.default(dep);
    routes.use('/customer', customerRouter);
    return routes;
};
exports.default = apiRouter;
