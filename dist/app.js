"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
-require('dotenv').config();
const body_parser_1 = __importDefault(require("body-parser"));
const dotenv = __importStar(require("dotenv"));
const express_1 = __importDefault(require("express"));
const ProjectDependencies = __importStar(require("./config/db"));
const error_handler_1 = __importDefault(require("./framework/common/error_handler"));
const index_1 = __importDefault(require("./framework/routes/web/index"));
dotenv.config({ path: __dirname + '../.env' });
const app = express_1.default();
const port = process.env.PORT || 3001;
ProjectDependencies.Mysql.authenticate().then(() => {
    app.use(body_parser_1.default.urlencoded({ extended: true }));
    app.use(body_parser_1.default.json());
    app.use(error_handler_1.default);
    app.use('/', index_1.default(ProjectDependencies));
    app.listen(port, () => {
        console.log('Connection has been established successfully.');
        console.log(`Customer-API | http://localhost:${port}`);
    });
}, (err) => {
    console.error('Unable to connect to the database:', err);
});
