"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DoGetCustomer = void 0;
const mysql_1 = __importDefault(require("../repository/mysql"));
//import { HttpCode } from '../../../framework/common/constant';
const DoGetCustomer = (repository) => {
    console.log("entry getcustomer get.ts");
    const Execute = (bearer, req) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            return yield repository.transaction((t) => __awaiter(void 0, void 0, void 0, function* () {
                const cusRepo = mysql_1.default(repository);
                const getCus = yield cusRepo.Read();
                return Promise.all([getCus]);
            }));
        }
        catch (err) {
            throw err;
        }
    });
    return {
        Execute,
    };
};
exports.DoGetCustomer = DoGetCustomer;
