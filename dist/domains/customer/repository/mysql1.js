"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
//import { Transaction } from 'sequelize/types';
const customer_1 = require("../../../models/customer");
const CustomerRepository = (repo) => {
    const repository = repo;
    const Read = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const cus = customer_1.CustomerFactory(repository);
        const custs = yield cus.findAll({});
        res.status(200).send(custs);
    });
    var Create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const cus = customer_1.CustomerFactory(repository);
        /*     const cust  = await cus.create({
                name:"Ratu",password:'123', email:'raja@gmail.com'
            })
            
         */
        //const cust : CustomerModel[] = await cusss.findAll({});
        //const t = await sequelize.Transaction();
        const cun = yield cus.create({ name: "Rara", password: '123', email: 'raja@gmail.com' });
        /* try {
        //  transaction:t
        console.log("Ara try");
        
        //t.commit();
        }catch(e){
            throw new Error
          //  t.rollback();
        }  */
        try {
            console.log("entering try block");
            throw "thrown message";
            console.log("this message is never seen");
        }
        catch (e) {
            console.log("entering catch block");
            console.log(e);
            console.log("leaving catch block");
        }
        finally {
            console.log("entering and leaving the finally block");
        }
        res.status(200).send(cun);
        return cun === null ? null : cun;
    });
    const ReadBy = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const cuss = customer_1.CustomerFactory(repository);
        const idx = req.params.id;
        const cuz = yield cuss.findOne({
            where: {
                id: idx
            }
        });
        res.status(200).send(cuz);
        return cuz === null ? null : cuz;
    });
    return {
        Read,
        ReadBy,
        Create
    };
};
exports.default = CustomerRepository;
