"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const customer_1 = require("../../../models/customer");
/* export type Param = {
    uid?: string;
    customerName?: string;
    customerEmail?: string;
    customerUid?: string;
    customerPhoneNumber?: string;
    order?: string;
    sort?: string;
    keyword?: string;
}; */
const CustomerRepository = (repo) => {
    const repository = repo;
    const Read = () => __awaiter(void 0, void 0, void 0, function* () {
        const cus = customer_1.CustomerFactory(repository);
        const cust = yield cus.findAll({});
        console.log(cust);
        return cust === null ? null : cust;
    });
    return {
        Read
    };
};
exports.default = CustomerRepository;
