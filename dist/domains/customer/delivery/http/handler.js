"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("../../use_cases/index");
//import { ResSingleData, ResSuccess, ResWithPagination } from '@framework/http/http_response';
const CustomerHttp = (dep) => {
    //const repository = dep.Mysql;
    const GetCustomer = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        //	console.log("entry handler function");
        //const GetData = DoGetCustomer(repository);
        //const GetData = DoGetCustomer(dep.Mysql)
        //const Data =  await GetData.Execute()
        const Data = yield index_1.DoGetCustomer(dep.Mysql).Execute();
        res.status(200).send(Data);
    });
    return {
        GetCustomer
    };
};
exports.default = CustomerHttp;
