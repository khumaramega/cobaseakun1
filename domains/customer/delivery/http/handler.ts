import * as ProjectDependencies from '../../../../config/db';
import {NextFunction, Request, Response} from 'express';
import  {DoGetCustomer} from '../../use_cases/index';

//import { ResSingleData, ResSuccess, ResWithPagination } from '@framework/http/http_response';

const CustomerHttp  = (dep: typeof ProjectDependencies) => {
	//const repository = dep.Mysql;
	const GetCustomer = async (req: Request, res: Response, next: NextFunction) => {
	//	console.log("entry handler function");
	//const GetData = DoGetCustomer(repository);
	//const GetData = DoGetCustomer(dep.Mysql)
	//const Data =  await GetData.Execute()
	const Data = await DoGetCustomer(dep.Mysql).Execute()
		res.status(200).send(Data); 
	};

   
	return {
		GetCustomer
	};


};

export default CustomerHttp ;
