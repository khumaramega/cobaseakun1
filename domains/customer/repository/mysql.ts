import sequelize, { Op, Sequelize } from 'sequelize';
import { CustomerAttributes, CustomerFactory, CustomerModel } from '../../../models/customer';

/* export type Param = {
	uid?: string;
	customerName?: string;
	customerEmail?: string;
	customerUid?: string;
	customerPhoneNumber?: string;
	order?: string;
	sort?: string;
	keyword?: string;
}; */

const CustomerRepository = (repo: Sequelize) => {
	const repository = repo;

	const Read = async (): Promise<Array<CustomerModel> | null> => {
		const cus = CustomerFactory(repository);

		const cust: CustomerModel[] = await cus.findAll({
			
		});
		console.log(cust)
		return cust === null ? null : cust;
	};

	
	return {
		Read
	};
};

export default CustomerRepository;

