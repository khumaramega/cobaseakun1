import { Sequelize, Transaction } from 'sequelize/types'; 
import CustomerRepo from '../repository/mysql';
import { CustomerModel } from '../../../models/customer';
//import { HttpCode } from '../../../framework/common/constant';


const DoGetCustomer = (repository: Sequelize) => {
	console.log("entry getcustomer get.ts");
	
	const Execute = async (bearer?: string, req?: any): Promise<any> => {
		try {
			return await repository.transaction(async (t: Transaction) => {
				const cusRepo = CustomerRepo(repository);
				const getCus: CustomerModel[] | null = await cusRepo.Read();

				return Promise.all([getCus]);
				
			});
		} catch (err) {
			throw err;
		}
	};

	return {
		Execute,
	};
};



export { DoGetCustomer};
