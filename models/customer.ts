import { BuildOptions, DataTypes, Model, Sequelize } from 'sequelize';

export type CustomerAttributes = {
	id?: number;
    name?: string;
    password?: string;
    email?: string;
};

export interface CustomerModel extends Model<CustomerAttributes>, CustomerAttributes {
}

export type CustomerStatic = typeof Model & {
	new(values?: object, options?: BuildOptions): CustomerModel;
};

export class Customer extends Model<CustomerModel, CustomerAttributes> {
}

export function CustomerFactory(sequelize: Sequelize): CustomerStatic {
	return <CustomerStatic>sequelize.define(
		'customer',
		{
			id: {
				field: 'id',
				type: DataTypes.INTEGER,
				autoIncrement: true,
                primaryKey: true,
                
			},
			
			name: {
				field: 'name',
				type: DataTypes.STRING,
				allowNull: true,
            },
            password: {
				field: 'password',
				type: DataTypes.STRING,
				allowNull: true
            },
            email: {
				field: 'email',
				type: DataTypes.STRING,
				allowNull: true
			},
			
		},
		{
			timestamps: false,
			freezeTableName: true,
		}
	);
}
