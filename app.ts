-require('dotenv').config();

import bodyParser from 'body-parser';
import * as dotenv from 'dotenv';
import express from 'express';
import * as ProjectDependencies from './config/db'
import ErrorHandler from './framework/common/error_handler';

import Routes from './framework/routes/web/index';
dotenv.config({ path: __dirname + '../.env' });


const app = express();
const port = process.env.PORT || 3001;

ProjectDependencies.Mysql.authenticate().then(
	() => {
		app.use(bodyParser.urlencoded({ extended: true }));
		app.use(bodyParser.json());
		app.use(ErrorHandler);
		app.use('/', Routes(ProjectDependencies));
		app.listen(port, () => {
			console.log('Connection has been established successfully.');
			console.log(`Customer-API | http://localhost:${port}`);
		});
	},
	(err: any) => {
		console.error('Unable to connect to the database:', err);
	}
);

