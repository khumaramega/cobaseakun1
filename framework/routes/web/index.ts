import * as ProjectDependencies from '../../../config/db';
import Customer from './customer';
//import Customer from './customerroutes';
import express from 'express';

const apiRouter = (dep: typeof ProjectDependencies) => {
	const routes = express.Router();
	const customerRouter = Customer(dep);

	routes.use('/customer', customerRouter);

	return routes;
};

export default apiRouter;


