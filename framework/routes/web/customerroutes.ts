import * as ProjectDependencies from '../../../config/db';
import CustomerRepository from '../../../domains/customer/repository/mysql1'
import CustomerHttp from '../../../domains/customer/delivery/http/handler';
import express from 'express';
//import {Request, Response} from 'express';



const PackageRouter = (dep: typeof ProjectDependencies) => {
	const router = express.Router();

 	const controller = CustomerRepository(dep.Mysql);

	router.route('/').get(controller.Read) 
	router.route('/:id').get(controller.ReadBy) 
	router.route('/cust').get(controller.Create)
	


	return router;
};

export default PackageRouter;


