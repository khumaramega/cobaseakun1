import * as ProjectDependencies from '../../../config/db';
import CustomerHttp from '../../../domains/customer/delivery/http/handler';
import express from 'express';


const PackageRouter = (dep: typeof ProjectDependencies) => {
	const router = express.Router();

 	const controller = CustomerHttp(dep);

	router.route('/').get(CustomerHttp(dep).GetCustomer) 
//	router.route('/:id').get(controller.ReadBy) 
	
	return router;
};

export default PackageRouter;


