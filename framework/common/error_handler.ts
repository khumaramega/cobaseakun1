import logger from './winston';
import { Error as seqErr } from 'sequelize';
import { HttpCode } from './constant';

const ErrorHandler = (err: any, req: any, res: any, next: any) => {
	let message, status, code;

	if (err instanceof seqErr) {
		if (process.env.DEBUG) {
			status = HttpCode.INTERNAL_SERVER_ERROR;
			message = err.message;
		} else {
            
			status = HttpCode.INTERNAL_SERVER_ERROR;
			message = 'internal server error';
		}
	} else {
		if (process.env.DEBUG) {
			status = err.status;
			message = err.message;
		} else {
			status = HttpCode.INTERNAL_SERVER_ERROR;
			message = 'oops something wrong';
		}
	}

	res.locals.message = message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	logger.error(`${status || 500} - ${message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

	res.status(status || 500);
	res.json({
		status: status || 500,
		message: message,
		code: code,
	});
};

export default ErrorHandler;
