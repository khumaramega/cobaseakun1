//simple promise biasa
/*  let done = false
const cobaRara = new Promise((resolve, reject) => {
    if (done) {
        const success = "Success!!!"
        resolve(success)
    } else {
        const why = "Failed!"
        reject(why)
    }
})
 cobaRara
 .then((data) => {
    console.log('Request succeeded with JSON response', data)
  })
 .catch((err) => {
     console.error(err);
 })   */



//promise.all, promise race
/* const recordVideo1 = new Promise((resolve, reject) =>{
  resolve('video1')  
})  

const recordVideo2 = new Promise((resolve,reject) => {
    resolve('video2')
})

const recordVideo3 = new Promise((resolve, reject) => {
    resolve('video3')
})

/* Promise.all([
    recordVideo1,
    recordVideo2,
    recordVideo3
]).then((messages) => {
    console.log(messages);
})  

Promise.race([
    recordVideo1,
    recordVideo2,
    recordVideo3
]).then((messages) => {
    console.log(messages);
})  */


//callback function

/* const userLeft = false;
const memeCute = false;

function tutorialCallback(callback, errorCallback){
if(userLeft){
    errorCallback({
        name: 'user Left',
        message: ':('
    })
} else if (memeCute) {
    errorCallback({
        name: 'user watch cat',
        message: 'nice'
    })
    }
else {
    callback('thumb up')
    }

}

tutorialCallback((message) => {
    console.log('success:' + message);
}, (error) => {

    console.log(error.name+ ' ' + error.message)
}) */


//change to PRomise
const userLeft = true;
const memeCute = false;

function tutorialCallback() {
    return new Promise((resolve, reject) => {
        if(userLeft){                                                                                                                                                               
            reject({
                name: 'user Left',
                message: ':('
            })
        } else if (memeCute) {
            reject({
                name: 'user watch cat',
                message: 'nice'
            })
            }
        else {
            resolve('thumb up')
            }
    
    })
}

 tutorialCallback()
 .then((message) => {
    console.log('success:' + message);
})
.catch((error) => {

    console.log(error.name+ ' ' + error.message)
})