import * as dotenv from "dotenv";
dotenv.config();

import { Sequelize } from 'sequelize';

const Mysql= new Sequelize(process.env.DB_NAME!, process.env.DB_USER!, process.env.DB_PWD, {
	host: process.env.DB_HOST,
	dialect: 'mysql',
	port: +process.env.DB_PORT!,
	pool: {
		min: 0,
		max: 5,
		acquire: 30000,
		idle: 10000,
	},
	logging: false,
});

export { Mysql };
